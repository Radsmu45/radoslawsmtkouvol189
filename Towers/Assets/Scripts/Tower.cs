﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

	public GameObject missilePrefab;
	public bool isActive;
	public Vector3 spawnPosition;
	void Start () {
		StartCoroutine(rotateAndShoot());
	}
	
	
	void Update () {
		
	}

	IEnumerator rotateAndShoot()
	{
		while (isActive)
		{
			yield return new WaitForSeconds(0.5f);
			float angle = Random.Range(15f, 45f);
			transform.rotation = Quaternion.Euler(0, 0, angle);

			Shoot();
		}
	}

	void Shoot()
    {
		var missile = Instantiate(missilePrefab);
		missile.transform.position = transform.position + transform.rotation * spawnPosition;

		var rigidbody = missile.GetComponent<Rigidbody2D>();
		rigidbody.velocity = transform.rotation * Vector3.forward * 5;
    }


}
