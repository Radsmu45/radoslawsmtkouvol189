﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy(gameObject, 3f);
	}
	
	// Update is called once per frame

	private void OnCollisionEnter(Collision collision)
    {
		if (collision.collider.name != "Tower") return;

		Destroy(collision.gameObject);
		Destroy(gameObject);
    }
}
